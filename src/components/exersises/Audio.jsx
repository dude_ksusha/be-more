import React from 'react';
import PininInput from './PininInput'
import { validateInput } from "../../helpers/pinin";

function Audio(props) {

  return (
    <div className='words'>
     
      {props.isAnswerShown ?
        <>{validateInput(props)}</>
        :
        <PininInput input={props.input} setInput={props.setInput} setIsAnswerShown={props.setIsAnswerShown}/>
      }
    </div>
  )
}

export default Audio
