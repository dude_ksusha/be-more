import React from 'react';

function Translation(props) {

  function validateInput() {
    return <>
      <p className='hieroglyph'>{props.phrase.hieroglyph}</p>
      <p className='pinin'>{props.phrase.pinin}</p>
      <p className='translation'>{props.phrase.translate}</p>
    </>
  }

  return (
    <div className='words'>
      {props.isAnswerShown ?
        <>{validateInput()}</>
        :
        <>
          <p className='hieroglyph'>{props.phrase.hieroglyph}</p>
          <p className='pinin'>{props.phrase.pinin}</p>
        </>
      }
    </div>
  )

}

export default Translation
